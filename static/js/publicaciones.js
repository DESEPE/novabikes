const app = document.getElementById('root');
var email = getCookie('email');
var usuarioActual = null;

const divForm = document.createElement('div');
divForm.setAttribute('class', 'fixed');
divForm.setAttribute('align', 'center');

const form = document.createElement('form');
form.setAttribute('action', 'javascript: void(0);');
form.setAttribute('onsubmit', 'publish()');

const input = document.createElement('textarea');
input.setAttribute('id', 'inputPublicar');
input.setAttribute('name', 'texto');
input.setAttribute('required', 'true');
input.setAttribute('maxlength', '140');
const br = document.createElement('br');
const submit = document.createElement('input');
const submitFile = document.createElement('input');
submit.setAttribute('id', 'button1');
submitFile.setAttribute('type', 'file');
submitFile.setAttribute('id', 'file');
submitFile.setAttribute('name', 'file')
submit.setAttribute('type', 'submit');
submit.setAttribute('value', 'Publicar');
var urlImg;

const table = document.createElement('table');
table.setAttribute('align', 'center');
const firstRow = document.createElement('tr');
const usuario = document.createElement('th');
usuario.textContent = "Usuario";
const texto = document.createElement('th');
texto.textContent = "Publicación";
const fecha = document.createElement('th');
fecha.textContent = "Fecha";
const imagen = document.createElement('th');
imagen.textContent = "Imagen";
const editDel = document.createElement('th');

form.appendChild(input);
form.appendChild(br);
form.appendChild(submit);
form.appendChild(submitFile);
divForm.appendChild(form);

firstRow.appendChild(usuario);
firstRow.appendChild(texto);
firstRow.appendChild(fecha);
firstRow.appendChild(imagen);
firstRow.appendChild(editDel);
table.appendChild(firstRow);

divForm.appendChild(table);
app.appendChild(divForm);

function init() {
    var request = createRequest();
    request.open('GET', 'server/usuarios/email/' + email + '/', true);
    request.setRequestHeader('Accept', 'application/json');

    request.onload = function() {
        if (request.readyState === 4 && request.status === 200) {
            usuarioActual = JSON.parse(this.response)[0];
            getPublicaciones();
        }
    };

    request.send();


}

function getPublicaciones() {
    while (table.hasChildNodes()) {
        table.removeChild(table.firstChild);
    }
    table.appendChild(firstRow);

    var request = createRequest();
    request.open('GET', 'server/publicaciones/', true);
    request.setRequestHeader('Accept', 'application/json');

    request.onload = function() {
        //alert(this.response);
        var data = JSON.parse(this.response);

        if (request.readyState === 4 && request.status === 200) {
            data.forEach(publicacion => {
                const row = document.createElement('tr');
                const tdUsuario = document.createElement('td');
                const tdTexto = document.createElement('td');
                const tdFecha = document.createElement('td');
                const tdImagen = document.createElement('td');

                var nombreUsuario = findUsuario(publicacion.idUsuario);
                tdUsuario.textContent = (nombreUsuario === null ? 'Anonymous' : nombreUsuario.usuario);
                tdTexto.textContent = publicacion.texto;
                tdFecha.textContent = publicacion.fecha.substr(0, 10);
                if (publicacion.img !== null) {
                    var enlace = document.createElement('a');
                    enlace.href = publicacion.img;
                    var img = document.createElement('img');
                    img.height = 100
                    img.src = publicacion.img;
                    enlace.appendChild(img);
                    tdImagen.appendChild(enlace);
                } else {
                    tdImagen.textContent = "";
                }
                row.appendChild(tdUsuario);
                //alert(usuarioActual);
                //alert(usuarioActual.id);
                if (publicacion.idUsuario === usuarioActual.id) {
                    const tdEditDel = document.createElement('td');
                    const taPubli = document.createElement('textarea');
                    taPubli.setAttribute('id', publicacion.id.toString());
                    taPubli.setAttribute('class', 'inputEditar');
                    taPubli.setAttribute('required', 'true');
                    taPubli.setAttribute('maxlength', '140');
                    taPubli.value = publicacion.texto;

                    bEdit = document.createElement('button');
                    bEdit.textContent = 'Editar';
                    bEdit.setAttribute('type', 'button');
                    bEdit.setAttribute('style', 'margin-right: 5%;');
                    bEdit.setAttribute('name', publicacion.id.toString());
                    bEdit.onclick = function() {
                        let bEditOfPubli = document.getElementsByName(publicacion.id.toString())[0];

                        if (bEditOfPubli.textContent === 'Confirmar') {
                            editPublicacion(publicacion);
                            getPublicaciones();
                        } else {
                            bEditOfPubli.textContent = 'Confirmar';
                            tdTexto.textContent = '';
                            tdTexto.appendChild(taPubli);
                        }
                    };

                    bDel = document.createElement('button');
                    bDel.textContent = 'Eliminar';
                    bDel.setAttribute('type', 'button');
                    bDel.setAttribute('onclick', 'javascript: deletePublicacion(' + JSON.stringify(publicacion) + '); getPublicaciones();');

                    row.appendChild(tdTexto);
                    row.appendChild(tdFecha);
                    row.appendChild(tdImagen);
                    tdEditDel.appendChild(bEdit);
                    tdEditDel.appendChild(bDel);
                    row.appendChild(tdEditDel);
                } else {
                    row.appendChild(tdTexto);
                    row.appendChild(tdFecha);
                    row.appendChild(tdImagen);
                    row.appendChild(document.createElement('td'));
                }

                table.appendChild(row);
            });
        } else {
            const errorMessage = document.createElement('marquee');
            errorMessage.textContent = 'Error';
            app.appendChild(errorMessage);
        }
    };

    request.send();
}

function findUsuario(idUser) {
    var request = createRequest();
    request.open('GET', 'server/usuarios/' + idUser, false);
    request.setRequestHeader('Accept', 'application/json');
    var data = null;

    request.onload = function() {
        if (request.readyState === 4 && request.status === 200) {
            data = JSON.parse(this.response);
        }
    };

    request.send();

    return data;
}

function editPublicacion(publi) {
    let taPubliOfPubli = document.getElementById(publi.id.toString());
    //alert(taPubliOfPubli);

    if (publi.texto !== taPubliOfPubli.value) {
        var request = createRequest();
        request.open('PUT', 'server/publicaciones/' + publi.id + '/', false);
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify({
            id: publi.id,
            texto: taPubliOfPubli.value,
            fecha: new Date().toISOString().substring(0, 10),
            idUsuario: usuarioActual.id
        }));
    }
}

function deletePublicacion(publi) {
    var request = createRequest();
    request.open('DELETE', 'server/publicaciones/' + publi.id + '/', false);
    request.send();
}

function postPublicacion() {

    var request = createRequest();
    request.open('POST', 'server/publicaciones/', false);
    request.setRequestHeader('Content-Type', 'application/json');
    if (urlImg !== null) {
        var object = JSON.stringify({
            texto: input.value,
            fecha: new Date().toISOString().substring(0, 10),
            idUsuario: usuarioActual.id,
            img: urlImg
        });
        request.send(object);
        location.reload();
    } else {
        var object = JSON.stringify({
            texto: input.value,
            fecha: new Date().toISOString().substring(0, 10),
            idUsuario: usuarioActual.id,
            img: null
        });
        request.send(object);
        location.reload();
    }
}

function subirfoto() {
    file = document.getElementById('file').files[0];
    if (file !== undefined) {
        // Replace ctrlq with your own API key
        var apiUrl = 'https://api.imgur.com/3/image';
        // Recommended to save this api key as enviroment variable (to not been
        // seen if someone inspects your code)
        var apiKey = '';
        var fd = new FormData();
        fd.append("image", file); // Append the file
        var xhr = new XMLHttpRequest();
        xhr.open("POST", apiUrl);
        xhr.setRequestHeader("Authorization", "Client-ID ".concat(apiKey));
        xhr.onload = function() {
            // Big win!
            // The URL of the image is:
            urlImg = JSON.parse(xhr.responseText).data.link;
            postPublicacion();
        }
        xhr.send(fd);
    } else {
        urlImg = null;
        postPublicacion();
    }
}

function publish() {
    subirfoto();
    //input.value = '';
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) === 0)
            return c.substring(name.length, c.length);
    }
    return "";
}

function createRequest() {
    var result = null;

    if (window.XMLHttpRequest) {
        result = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        result = new ActiveXObject("Microsoft.XMLHTTP");
    } else {
        alert("Funcionalidad no soportada por el navegador");
    }

    return result;
}