# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_python37_render_template]
import datetime

from flask import Flask, render_template, request
from google.auth.transport import requests
from google.cloud import datastore
import google.oauth2.id_token
import json
import math
import requests as requests2

app = Flask(__name__)

datastore_client = datastore.Client()

# Definicion de metodos para endpoints

# Definicion de las url del cliente
@app.route('/')
def root():
    return render_template('index.html')

@app.route('/initial_page')
def initial_page():
    return render_template('initial_page.html')

@app.route('/navbar')
def navbar():
    return render_template('navbar.html')

@app.route('/mapa')
def mapa():
    return render_template('mapa.html')

@app.route('/publicaciones')
def publicacionesRender():
    return render_template('publicaciones.html')

@app.route('/perfil')
def perfil():
    return render_template('perfil.html')

@app.route('/busqueda')
def busqueda():
    return render_template('busqueda.html')

@app.route('/check_token')
def check_token():
    return render_template('check_token.html')


# Paths asociados a Usuarios
# Devuelve todos los usuarios
@app.route('/server/usuarios/', methods=['GET'])
def usuarios():
    query = datastore_client.query(kind='Usuarios')
    return json.dumps(list(query.fetch()))

def getKey(table, id):
    query = datastore_client.query(kind=table)
    query.add_filter('id', '=', id)
    lista = list(query.fetch())
    if len(lista) > 0:
        user = lista[0]
        return user.key
    else:
        return None

# Devuelve, actualiza o elimina un usuario con la id especificada
@app.route('/server/usuarios/<id>/', methods=['GET', 'PUT', 'DELETE'])
def usuario(id):
    if request.method == 'DELETE':
        key = getKey('Usuarios', id)
        if key != None:
            datastore_client.delete(key)
        return ('', 200)
    elif request.method == 'PUT':
        data = request.json
        with datastore_client.transaction():
            key = getKey('Usuarios', id)
            if key != None:
                key = datastore_client.key('Usuarios', key.id)
                user = datastore_client.get(key)

                user['usuario'] = data['usuario']
                user['nombre'] = data['nombre']
                user['birth_date'] = data['birth_date']

                datastore_client.put(user)
        return ('', 200)
    else:
        query = datastore_client.query(kind='Usuarios')
        query.add_filter('id', '=', id)
        lista = list(query.fetch())
        if len(lista) > 0:
            return json.dumps(lista[0])
        else:
            return json.dumps(lista)

# Devuelve el número de usuarios
@app.route('/server/usuarios/count/', methods=['GET'])
def usuariosCount():
    lista = json.loads(usuarios())
    return str(len(lista))

# Buscar usuario por su nombre
@app.route('/server/usuarios/username/<username>/', methods=['GET'])
def usuariosByUsername(username):
    lista = json.loads(usuarios())
    for user in lista:
        if username.lower() not in user['usuario'].lower():
            lista.remove(user)
    return json.dumps(lista)


# Buscar usuario por su email exacto
@app.route('/server/usuarios/email/<email>/', methods=['GET'])
def usuariosByEmail(email):
    query = datastore_client.query(kind='Usuarios')
    query.add_filter('email', '=', email)
    lista = list(query.fetch())
    return json.dumps(lista)


# Paths asociados a publicaciones
# Devuelve todas las publicaciones
@app.route('/server/publicaciones/', methods=['GET', 'POST'])
def publicaciones():
    if request.method == 'POST':
        data = request.json
        with datastore_client.transaction():
            incomplete_key = datastore_client.key('Publicaciones')
            
            pub = datastore.Entity(key=incomplete_key)
            
            query = datastore_client.query(kind='Publicaciones')
            query.projection = ['id']
            query.order = ['-id']
            lista = list(query.fetch(limit=1))
            if len(lista) == 0:
                id = 1
            else:
                id = int(lista[0]['id']) + 1

            pub.update({
                'id': id,
                'texto': data['texto'],
                'fecha': data['fecha'],
                'idUsuario': data['idUsuario'],
                'img': data['img']
            })
            
            datastore_client.put(pub)
            return ('', 200)
    else:
        query = datastore_client.query(kind='Publicaciones')
        query.order = ['-id']
        return json.dumps(list(query.fetch()))

# Devuelve, actualiza o elimina una publicación con la id especificada
@app.route('/server/publicaciones/<id>/', methods=['GET', 'PUT', 'DELETE'])
def publicacion(id):
    if request.method == 'DELETE':
        key = getKey('Publicaciones', int(id))
        if key != None:
            datastore_client.delete(key)
        return ('', 200)
    elif request.method == 'PUT':
        data = request.json
        with datastore_client.transaction():
            key = getKey('Publicaciones', int(id))
            if key != None:
                key = datastore_client.key('Publicaciones', key.id)
                pub = datastore_client.get(key)

                pub['texto'] = data['texto']
                pub['fecha'] = data['fecha']

                datastore_client.put(pub)
        return ('', 200)
    else:
        query = datastore_client.query(kind='Publicaciones')
        query.add_filter('id', '=', int(id))
        lista = list(query.fetch())
        if len(lista) > 0:
            return json.dumps(lista[0])
        else:
            return json.dumps(lista)

# Devuelve el número de usuarios
@app.route('/server/publicaciones/count/', methods=['GET'])
def publicacionesCount():
    lista = json.loads(publicaciones())
    return str(len(lista))

# Obtener publicaciones a partir de una subcadena del texto
@app.route('/server/publicaciones/filter/<filter>/', methods=['GET'])
def publicacionesFilter(filter):
    lista = json.loads(publicaciones())
    for pub in lista:
        if filter.lower() not in pub['texto'].lower():
            lista.remove(pub)
    return json.dumps(lista)

# Obtener publicaciones de un usuario a partir de su nombre de usuario
@app.route('/server/publicaciones/username/<username>/', methods=['GET'])
def publicacionesByUsername(username):
    user = json.loads(usuariosByUsername(username))
    if len(user) == 0:
        user = ''
    else:
        user = user['id']
    query = datastore_client.query(kind='Publicaciones')
    query.add_filter('idUsuario', '=', user)
    lista = list(query.fetch())
    return json.dumps(lista)

# Obtener publicaciones a partir de una fecha
@app.route('/server/publicaciones/date/<date>/', methods=['GET'])
def publicacionesByDate(date):
    query = datastore_client.query(kind='Publicaciones')
    query.add_filter('fecha', '=', date)
    lista = list(query.fetch())
    return json.dumps(lista)


#Datos abiertos

def estaEnRadio(latAp, lonAp, latCentro, lonCentro, radio):
    return (distancia(latAp, lonAp, latCentro, lonCentro) <= radio)

def distancia(lat1, lon1, lat2, lon2):
    lat1 = math.radians(lat1)
    lon1 = math.radians(lon1)
    lat2 = math.radians(lat2)
    lon2 = math.radians(lon2)
    earthRadius = 6371.01 #Kilometers
    return earthRadius * math.acos(math.sin(lat1) * math.sin(lat2) + math.cos(lat1) * math.cos(lat2) * math.cos(lon1 - lon2)) * 1000.0

#Paths asociados a los aparcamientos de bicicletas
# Devolver aparcamientos a partir de una posición geográfica
@app.route('/server/datosAbiertos/aparcamientos/<lat>/<lon>/<radius>/', methods=['GET'])
def aparcamientosEnRadio(lat, lon, radius):
    # En la cabecera del metodo ponemos a None lat,lon,rad para poder llamar al metodo sin estos parametros
    # y que estos tengan en ese caso el valor por defecto de null
    url = 'https://datosabiertos.malaga.eu/recursos/transporte/trafico/da_aparcamientosBici-4326.geojson'
    response = requests2.get(url)
    if response.status_code != 200:
        raise Exception('Fallo al conectar con la api externa')
    else:
        # Creamos la lista que devolvemos en formato json
        lista = {
            'type': 'FeatureCollection',
            'features': []
        }

        data = response.json()['features']  # Obtenemos los datos de la api e iteramos en ellos
        for feature in data:
            loon = float(feature['geometry']['coordinates'][0])
            laat = float(feature['geometry']['coordinates'][1])
            # Nos quedamos con los aparcamientos dentro del radio
            if estaEnRadio(laat, loon, float(lat), float(lon), float(radius)):
                feature = {  # Creamos la lista de cada elemento (feature)
                    'type': 'feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [loon, laat]
                    }
                }
                lista['features'].append(feature)
        return json.dumps(lista)

# Devolver el aparcamiento de bicicletas más cercano a partir de una posición geográfica
@app.route('/server/datosAbiertos/aparcamientos/<lat>/<lon>/', methods=['GET'])
def aparcamientoMasCercano(lat, lon):
    # En la cabecera del metodo ponemos a None lat,lon,rad para poder llamar al metodo sin estos parametros
    # y que estos tengan en ese caso el valor por defecto de null
    url = 'https://datosabiertos.malaga.eu/recursos/transporte/trafico/da_aparcamientosBici-4326.geojson'
    response = requests2.get(url)
    if response.status_code != 200:
        raise Exception('Fallo al conectar con la api externa')
    else:
        # Creamos el feature que devolvemos en formato json
        mejor = {
            'type': 'feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [200.0, 200.0]
            }
        }

        data = response.json()['features']  # Obtenemos los datos de la api e iteramos en ellos
        for feature in data:
            loon = float(feature['geometry']['coordinates'][0])
            laat = float(feature['geometry']['coordinates'][1])
            coordMejor = mejor['geometry']['coordinates']
            
            # Nos quedamos con los aparcamientos dentro del radio
            if distancia(laat, loon, float(lat), float(lon)) < distancia(coordMejor[1], coordMejor[0], float(lat), float(lon)):
                mejor = {  # Creamos la lista de cada elemento (feature)
                    'type': 'feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [loon, laat]
                    }
                }
        return json.dumps(mejor)

#Paths asociados a los bike-sharing
# Devolver aparcamientos a partir de una posición geográfica
@app.route('/server/datosAbiertos/bikesharing/<lat>/<lon>/<radius>/', methods=['GET'])
def bikesharingEnRadio(lat, lon, radius):
    # En la cabecera del metodo ponemos a None lat,lon,rad para poder llamar al metodo sin estos parametros
    # y que estos tengan en ese caso el valor por defecto de null
    url = 'https://datosabiertos.malaga.eu/recursos/transporte/EMT/EMTubicaparcbici/da_aparcamientosBiciEMT-4326.geojson'
    response = requests2.get(url)
    if response.status_code != 200:
        raise Exception('Fallo al conectar con la api externa')
    else:
        # Creamos la lista que devolvemos en formato json
        lista = {
            'type': 'FeatureCollection',
            'features': []
        }

        data = response.json()['features']  # Obtenemos los datos de la api e iteramos en ellos
        for feature in data:
            loon = float(feature['geometry']['coordinates'][0])
            laat = float(feature['geometry']['coordinates'][1])
            # Nos quedamos con los aparcamientos dentro del radio
            if estaEnRadio(laat, loon, float(lat), float(lon), float(radius)):
                feature = {  # Creamos la lista de cada elemento (feature)
                    'type': 'feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [loon, laat]
                    }
                }
                lista['features'].append(feature)
        return json.dumps(lista)

# Devolver el aparcamiento de bicicletas más cercano a partir de una posición geográfica
@app.route('/server/datosAbiertos/bikesharing/<lat>/<lon>/', methods=['GET'])
def bikesharingMasCercano(lat, lon):
    # En la cabecera del metodo ponemos a None lat,lon,rad para poder llamar al metodo sin estos parametros
    # y que estos tengan en ese caso el valor por defecto de null
    url = 'https://datosabiertos.malaga.eu/recursos/transporte/EMT/EMTubicaparcbici/da_aparcamientosBiciEMT-4326.geojson'
    response = requests2.get(url)
    if response.status_code != 200:
        raise Exception('Fallo al conectar con la api externa')
    else:
        # Creamos el feature que devolvemos en formato json
        mejor = {
            'type': 'feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [200.0, 200.0]
            }
        }

        data = response.json()['features']  # Obtenemos los datos de la api e iteramos en ellos
        for feature in data:
            loon = float(feature['geometry']['coordinates'][0])
            laat = float(feature['geometry']['coordinates'][1])
            coordMejor = mejor['geometry']['coordinates']
            
            # Nos quedamos con los aparcamientos dentro del radio
            if distancia(laat, loon, float(lat), float(lon)) < distancia(coordMejor[1], coordMejor[0], float(lat), float(lon)):
                mejor = {  # Creamos la lista de cada elemento (feature)
                    'type': 'feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [loon, laat]
                    }
                }
        return json.dumps(mejor)


# Autoriza el token de inicio de sesion
@app.route('/server/tokensignin', methods=['POST'])
def tokensignin():
    token = request.form['token']
    # ID del cliente de OAuth de google:
    CLIENT_ID = ''
    # Introduce aquí tu ID de Cliente Oauth. Si no tienes ninguna, debes generarlo desde
    # el panel de herramientas de desarrollador de Google.

    try:
        idinfo = google.oauth2.id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            return ('Error al comprobar el usuario en la base de datos', 401)

        # El usuario es correcto
        userid = idinfo['sub']  # Id del usuario

        # Vemos si el usuario existe en la base de datos y si no esta lo creamos
        # POR HACER!!!
        if len(json.loads(usuario(userid))) == 0:
            with datastore_client.transaction():
                incomplete_key = datastore_client.key('Usuarios')

                user = datastore.Entity(key=incomplete_key)

                user.update({
                    'id': userid,
                    'usuario': idinfo['email'],
                    'email': idinfo['email'],
                    'nombre': idinfo['name']
                })

                datastore_client.put(user)
                return ('', 200)
        
        # Devolvemos la respuesta al cliente
        return ('', 200)

    except ValueError:
        return ('Usuario erroneo', 401)


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='127.0.0.1', port=80, debug=True)
# [START gae_python37_render_template]
