# Novabikes #

**Creado por**:

* **Álvaro Castillo García**
* **Jose Antonio Escobar Bueno**
* **Manuel Esquivel Marín**
* **Ezequiel Rodríguez Márquez**
* **Daniel Cruzado Poveda**

### Notas previas ###

La liberación de este código se ha realizado con un objeto puramente educativo, para comprender ciertos aspectos e ideas  
y poder detectar ciertos errores que puedan servir como experiencia a la hora de crear proyectos. No es perfecta, y puede  
contener errores en ciertos aspectos, o prácticas que no sean del todo adecuadas. El código se da como tal y **ningún miembro  
del grupo se hace responsable del mal uso que se le dé a la aplicación**.

### Acerca del proyecto ###

Novabikes es un proyecto creado como práctica para el uso de APIs y servicios REST. La aplicación hace uso de  
los datos abiertos que se proporcionan de Málaga sobre aparcamientos de bicicletas, puntos "bike-sharing" y demás.  
Existen dos versiones de desarrollo de la herramienta, siendo la primera de ellas desarrollada con JSP y la segunda  
(esta que se proporciona) usando el framework de **Flask (Python)**. Las otras tecnologías utilizadas han sido **HTML** para la  
construcción de páginas, **CSS** para el apartado visual y **Javascript** para atender peticiones y tratar datos dinámicos  
desde el lado del servidor. También se hace uso de **Google App Engine**, como proveedor cloud para desplegar la aplicación   
y de **Google Cloud Datastore** para el almacenamiento de los datos del servidor. Las APIs utilizadas han sido:  

* **API de Imgur** para las publicaciones de fotografías [API de Imgur (click para verla)](https://apidocs.imgur.com/?version=latest)
* **API de Datos Abiertos de Málaga**, en concreto, los datos de bicicletas [Datos abiertos de Málaga (click para ver)](https://datosabiertos.malaga.eu/)
* **API de OpenLayers** para uso de mapa y de geolocalización de puntos en el mismo. [API de OpenLayers (click para verla)](https://openlayers.org/)
* **API de OpenWeather** para obtención de datos meteorológicos (hace uso de la localización actual del usuario). [API de OpenWeather (click para verla)](https://openweathermap.org/)


### Requisitos de instalación ###

Para poder instalar esta aplicación y realizar pruebas locales, será necesario tener instalado Python. Además, la aplicación  
hace uso de una clave de API para las distintas APIs especiicadas. Para Google la clave se utiliza para el inicio de sesión (**OAuth  
de Google**), por lo que habremos de registrarla en nuestro panel de desarrollador de Google. Para el resto hay que generarlas en sus.  
distintas páginas. **Este proyecto NO PROPORCIONA NINGUNA DE ESTAS CLAVES**.

### Dependencias a instalar en Python ###

Las dependencias las podemos instalar directamente de con estos comandos por consola: 

- curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
- python get-pip.py
- python -m pip install Flask
- pip install requests
- pip install google-api-python-client
- pip install google-cloud-datastore


### Memoria ###

Se adjunta una breve memoria técnica que explica los distintos aspectos de la aplicación:
[Haz click para ver la memoria](https://drive.google.com/file/d/1iao-fOd_iruG7k0r_MLZrJs0nlpUf_v6/view?usp=sharing)

### Contacto ###

[Mi Linkedin (click para ver perfil)](https://www.linkedin.com/in/daniel-cruzado-poveda-2824191a5/)

** Agradecimientos a mis compañeros de carrera por la colaboración de este proyecto **